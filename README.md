__WELCOME TO DUKE TIP/PARI 2014 Into to Programming and Data Analysis!__  
  
_WHAT LIES HEREIN:_  
  
- intro_to_programming  
  - contains the IPython Notebook script for the Intro to Programming lectures (_NOTE: IPython Notebook scripts have .ipynb extensions_)
  
- stats  
  - contains the .ipynb script for the Stats and Data Analysis lecture


The BitBucket repository for these lectures is at URL:
https://ihavehands@bitbucket.org/ihavehands/duke_tip_pari_repo_2014.git

The MoPad for these lectures (_for sharing code and comments_) is at URL:
https://etherpad.mozilla.org/okYGeYWGdw
